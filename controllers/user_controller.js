var requestJSON = require('request-json');
const URL_MYDB='https://api.mlab.com/api/1/databases/techu15db/collections/';
const clienteMlab = requestJSON.createClient(URL_MYDB);
require('dotenv').config();
const mapiKey='apiKey=' + process.env.mapiKey ;
var queryStrField ='f={"_id":0}&';

//Method POST login
function postLogin(req, res){
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
   let limFilter = 'l=1&';
   clienteMlab.get('user?'+ queryString + limFilter + mapiKey,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) { // Existe un usuario que cumple 'queryString'
           let login = '{"$set":{"logged":true}}';
           clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + mapiKey, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
};
module.exports.postLogin = postLogin;

//Method POST logout
 function postLogout(req, res){
   let email = req.body.email;
   let queryString = 'q={"email":"' + email + '"}&';
   let limFilter = 'l=1&';
   clienteMlab.get('user?'+ queryString + limFilter + mapiKey,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) { // Existe un usuario que cumple 'queryString'
           let logout = '{"$unset":{"logged":true}}';
           clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + mapiKey, JSON.parse(logout),
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Logout correcto', 'user':body[0].email, 'userid':body[0].id});
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
};
module.exports.postLogout = postLogout;

//DELETE user with id eliminar usuario
function deleteDelUsers(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';
   clienteMlab.get('user?' +  queryStringID + mapiKey,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
         console.log(respuesta._id);
         clienteMlab.delete(URL_MYDB + "user/" + respuesta._id.$oid +'?'+ mapiKey,
         function(error, respuestaMLab,body){
           res.send(body);
       });
     });
 };
module.exports.deleteDelUsers = deleteDelUsers;

// POST 'users' mLab
function postInsUsers(req, res){
   clienteMlab.get('user?' + mapiKey,
     function(error, respuestaMLab, body){
       newID = body.length + 1;
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       clienteMlab.post(URL_MYDB + "user?" + mapiKey, newUser,
         function(error, respuestaMLab, body){
           console.log(body);
           res.status(201);
           res.send(body);
         });
     });
 };
module.exports.postInsUsers = postInsUsers;

//PUT users con parámetro 'id' actualiza un user
function putUsers(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 clienteMlab.get('user?'+ queryStringID + mapiKey,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    clienteMlab.put(URL_MYDB +'user?' + queryStringID + mapiKey, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
      res.send(body);
     });
   });
};
module.exports.putUsers = putUsers;

// GET users a través de mlab, obtiene los datos del cliente
function getUsers(req,res){
    var id = req.params.id;
    var queryString= 'q={"id":'+ id +'}&';
      clienteMlab.get('user?' + queryString + queryStrField + mapiKey,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            };
            res.status(404);
          }
        }
        res.send(response);
      });
};
module.exports.getUsers = getUsers;
