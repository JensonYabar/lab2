# Imagen docker base inicial
FROM node:latest
# Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-api
# Copiar archivos del proyecto en directorio de Docker
ADD . /docker-api
# Instalar las dependencias del proyecto en producción
# RUN npm install --production
#                 --only=production
# Puerto donde exponemos nuestro contenedor (mismo que definimos en nuestra API)
EXPOSE 3000
# Lanzar la  aplicación (appe.js)
# CMD ["npm", "run", "pro"]
CMD ["npm", "start"]
