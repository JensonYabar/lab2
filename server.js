var express = require('express');
var userFile = require('./users.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE ='/apitechu/v1/';
const URL_MYDB='https://api.mlab.com/api/1/databases/techu15db/collections/';
//const mapiKey='apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

const PORT = process.env.PORT || 3000;
var queryStrField ='f={"_id":0}&';
//https://api.mlab.com/api/1/databases/techu15db/collections/user?f={"_id":0}&apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
const user_controller = require('./controllers/user_controller.js');
app.get(URL_BASE + 'users/:id', user_controller.getUsers);
app.put(URL_BASE + 'users/:id', user_controller.putUsers);
app.post(URL_BASE + 'users', user_controller.postInsUsers);
app.delete(URL_BASE + 'users/:id', user_controller.deleteDelUsers);
app.post(URL_BASE + 'logout', user_controller.postLogout);
app.post(URL_BASE + 'login', user_controller.postLogin);

// Petición PUT con id de mLab (_id.$oid)
 app.put(URL_BASE + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(URL_MYDB);
     httpClient.get('user?' + queryString + mapiKey,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + mapiKey, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });



// // GET users a través de mLab
// app.get(URL_BASE + 'users/:id',
//   function(req, res) {
//     console.log('GET /apitechu/v1/users/:id');
//     var id = req.params.id;
// //    var queryString= 'q={"id":'+ id +'}&';
//     var queryString= 'q={"accountID":'+ id +'}&';
//
//     console.log(queryString);
//     console.log('user?' + queryString + mapiKey);
//     var httpClient = requestJSON.createClient(URL_MYDB);
//     console.log("Cliente HTTP mLab creado.");
// //    httpClient.get('user?' + queryString + mapiKey,
// //      httpClient.get('user?' + queryString + queryStrField + mapiKey,
//       httpClient.get('account?' + queryStrField + queryString + mapiKey,
//       function(err, respuestaMLab, body) {
//         console.log('Error: ' + err);
//         console.log('Respuesta MLab: ' + respuestaMLab);
//         console.log('Body: ' + body);
//         // respuesta = !err ? body : {"msg" : "Error al recuperar users de mLab."};
//         var response = {};
//         if(err) {
//             response = {
//               "msg" : "Error obteniendo usuario."
//             }
//             res.status(500);
//         } else {
//           if(body.length > 0) {
//             response = body;
//           } else {
//             response = {
//               "msg" : "Usuario no encontrado."
//             };
//             res.status(404);
//           }
//         }
//         res.send(response);
//       });
// });

// Petición GET con Query String (req.query)
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
// app.post(URL_BASE + 'users',
//   function(req, res) {
//     var newID = usersFile.length + 1;
//     var newUser = {
//       "id" : newID,
//       "first_name" : req.body.first_name,
//       "last_name" : req.body.last_name,
//       "email" : req.body.email,
//       "country" : req.body.country
//     };
//     usersFile.push(newUser);
//     console.log(usersFile);
//     res.send({"msg" : "Usuario creado correctamente: ", newUser});
//   });








app.get(URL_BASE + 'logados',
      function(request,response){
        let usuarios = [];
        let pos;
        for(us in userFile){
           let persona = {
             first_name : userFile[us].first_name,
             email : userFile[us].email
           }
           let logged = userFile[us].logged;
           if(logged != undefined ){
                usuarios.push(persona);
          }
        }
        response.send(JSON.stringify(usuarios));
});
//deslogueo

app.post(URL_BASE + 'users/logout', function(req,res){
  let credencial = {
    email : req.body.email
  }
  let userMail = credencial.email;
  let indFind;
  let valido = JSON.stringify(userMail);
  for(us in userFile){
     if(userFile[us].email == userMail){
       indFind = us;
     }
  }
  if(indFind != undefined){
    if(userFile[indFind].logged != undefined){
        if(userFile[indFind].logged == true){
          delete userFile.logged;
          res.send("deslogado con exito");
        }else{res.send("no esta logado");
          }
    }else{ res.send("no esta logado")}
  }else{
    res.send("no esta logado");
  }
});
//logueo
app.post(URL_BASE + 'users/login', function(req,res){
  if(reqEmpty(req)){
    let usuarios = [];
    let credencial = {
      email : req.body.email,
      password : req.body.password
    }
    let indFind;
    let userMail = credencial.email;
    let userPassword = credencial.password;
    let valido = JSON.stringify(credencial);
    for(us in userFile){
       let persona = {
         email : userFile[us].email,
         password : userFile[us].password
       }
       if(userFile[us].email == userMail ){
            indFind = us;
      }
       usuarios.push(persona);
    }
      if(indFind != undefined){
        if(userFile[indFind].password == userPassword){
          userFile[indFind].logged = true;
          writeUserDataToFile(userFile);
          res.status(200);
          res.send("logado")
        }else{
          res.status(210);
          res.send("no logado");
      }}else{ res.send("Usuario no existe")}
  }else{
    res.send("cuerpo vacio");
  }});


//Peticion GET de todos los users (Collections)
// app.get(URL_BASE + 'users',
//       function(request,response) {
//         console.log('GET ' + URL_BASE + 'users')
//         response.send(userFile);
//       });
// //Peticion GET de un users (Instance)
// app.get(URL_BASE + 'users/:id',
//       function(request,response){
//         console.log('GET' + URL_BASE + 'users/id');
//         let indice = request.params.id;
//         let instancia = userFile[indice - 1];
//         console.log(instancia);
//         let respuesta = (instancia != undefined) ? instancia :{"mensaje":"Recurso no encontrado."}
//         response.status(222);
//         response.send(respuesta);
// });
//peticion get con Query
app.get(URL_BASE + 'usersq',function(request,response){
  console.log('GET' + URL_BASE + 'con query');
  console.log(request.query);
  response.send(respuesta);
});
app.post(URL_BASE + 'users',
    function(req,res){
    totalUsers = userFile.length;
//    if(cuerpo.length > 0)
    console.log(req.body.length);
    let newUser = {
      userID : totalUsers++,
      first_name : req.body.first_name,
      last_name : req.body.last_Name,
      email : req.body.email,
      password : req.body.password
    }
    userFile.push(newUser);
    res.status(200);
    res.send({"mensaje" : "Usuario creado con exito.","usuario": newUser})
});
/*
app.put(URL_BASE + 'users/:id',
  function(req,res){
    var cntBody = Object.keys(req.body).length;
    if(cntBody > 0) {
      let idx = req.params.id - 1
      if(userFile[idx] != undefined){
      userFile[idx].first_name = req.body.first_name;
      userFile[idx].last_name = req.body.last_name;
      userFile[idx].email = req.body.email;
      userFile[idx].password= req.body.password;
      res.status(202);
      res.send({"mensaje" : "Usuario actualizado con exito.",
             "Array" : userFile[idx]});
      console.log(userFile[idx]);
      }else{
        res.send({"mensaje":"Recurso no encontrado."})
        }
    }else {
      let varError = {"mensaje" : "No Existe parametros para Body"};
      res.send(varError)
      console.log(varError);
      }
  });
*/
app.put(URL_BASE + 'users/:id',
  function(req, res){
    var cntBody = Object.keys(req.body).length;
    if(cntBody > 0){
      let idx = req.params.id - 1;
      let pos = 5; //userFile.find(userFile.userID == idx)
      userFile.splice(pos ,1);
      res.send(userFile);//{"Elemento eliminado:" :pos});
    }
  });

  // app.delete(URL_BASE + 'users', function(req, res) {
  //     if(reqEmpty(req)){
  //         console.log('Usuario a eliminar ' + JSON.stringify(req.body.userID));
  // userFile.splice(req.body.userID - 1, 1);
  //         res.status(200);
  //         res.send({"mensaje":"usuario eliminado" + JSON.stringify(userFile) });
  //     } else {
  //         res.send({ "mensaje": "No se tiene body" });
  //     }
  // });

  function reqEmpty(req) {
      return Object.keys(req.body).length !== 0 ? true : false;
  };

  function writeUserDataToFile(data){
    var fs
  };

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };


app.listen(PORT,function(){
  console.log('APITechU cambio A escuchando en puerto 3000...');
});
