var express = require('express');
var userFile = require('./users.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE ='/apitechu/v1/';
const URL_MYDB='https://api.mlab.com/api/1/databases/techu15db/collections/';
const PORT = process.env.PORT || 3000;
var queryStrField ='f={"_id":0}&';
const user_controller = require('./controllers/user_controller.js');

app.get(URL_BASE + 'users/:id', user_controller.getUsers);
app.put(URL_BASE + 'users/:id', user_controller.putUsers);
app.post(URL_BASE + 'users', user_controller.postInsUsers);
app.delete(URL_BASE + 'users/:id', user_controller.deleteDelUsers);
app.post(URL_BASE + 'logout', user_controller.postLogout);
app.post(URL_BASE + 'login', user_controller.postLogin);

//Peticion GET de todos los users (Collections)
app.get(URL_BASE + 'users',
    function(request,response) {
       console.log('GET ' + URL_BASE + 'users')
         response.send(userFile)
       });


function reqEmpty(req) {
    return Object.keys(req.body).length !== 0 ? true : false;
};

function writeUserDataToFile(data){
    var fs
};

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };

app.listen(PORT,function(){
  console.log('APITechU cambio A escuchando en puerto 3000...');
});
