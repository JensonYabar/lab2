//importamos framework de pruebas unitarias
var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

describe('Primer test desde mocha chai',
  function(){//funcion manejadora de la suite
    it('Test de que hay conexcion',//test unitario
     function(done){
       //chai.request('http://www.google.com')
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')//establece prueba para el get
       .end(
         function(err, res){//definicion de las aserciones
          res.should.have.status(200);
          res.body.should.be.a('array');
          for (users of res.body){
            users.should.have.property('first_name');
            users.should.have.property('last_name');
          }
          done();//terminamos de evaluar las aserciones
       })
     })
   });

  //  escribe('Primer test suite de TechU',
  //   function() { //Función manejadora de la suite
  //     it ('Prueba que la API devuelve una lista de usuarios correctos', //Test unitario
  //       function(done){
  //           chai.request('http://localhost:3000')
  //               .get('/colapi/v3/users')
  //               .end(
  //                 function(err, res){
  //                   res.should.have.status(200);
  //                   res.body.should.be.a('array');
  //                   for (users of res.body){
  //                     users.should.have.property('firstname');
  //                     users.should.have.property('last_name');
  //                   }
  //                   done();
  //                 })
  //       })
  //   });
   //
   //
